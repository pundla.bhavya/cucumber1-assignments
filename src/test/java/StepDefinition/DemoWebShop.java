package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoWebShop {

	static WebDriver driver=null;
	@Given("user is on Home Page")
	
	
	public void user_is_on_Home_Page() {
		driver=new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
	   // System.out.println("opening the browser");
	
	}

	@When("user click on login button")
	public void user_click_on_login_button() {
	    driver.findElement(By.xpath("//a[@class='ico-login']")).click();
		//System.out.println("click on login button");
	}

	@When("user dshould be navigate to login page")
	public void user_dshould_be_navigate_to_login_page() {
	    System.out.println("Login page will displayed");
	}

	@When("user enter email  and password")
	public void user_enter_email_and_password() {
		driver.findElement(By.id("Email")).sendKeys("pundlabhavya123@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("123456");
	   //System.out.println("enter email and password");
	}
	
	@When("user click on submit button")
	public void user_click_on_submit_button() {
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();
		//System.out.println("Click on submit button");
	}

	@Then("user should get logged in")
	public void user_should_get_logged_in() {
	    System.out.println("navigate to home page");
	 
	}


}
